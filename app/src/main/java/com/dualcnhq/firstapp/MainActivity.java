package com.dualcnhq.firstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private EditText txtUsername;
    private EditText txtPassword;

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        btnLogin.setOnClickListener(this);
        /*
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Button clicked");
            }
        });
        */

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
//                Log.d(TAG, "Button clicked");
                String userName = txtUsername.getText().toString();
                String password = txtPassword.getText().toString();

                Log.d(TAG,"userName: " + userName +  " password: " + password);

                if (userName.equals("admin") && password.equals("admin")) {
                    Toast.makeText(getApplicationContext(), "Yehey!!!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));

                } else {
                    Toast.makeText(getApplicationContext(), "Wrong credentials!!!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
}
